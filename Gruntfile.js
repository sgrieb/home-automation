/**
 * Created by Steve on 11/2/2015.
 */
module.exports = function(grunt) {

    var url = require("url"),
    fs = require("fs");
    
    // The default file if the file/path is not found
    var defaultFile = "index.html"

    // I had to resolve to the previous folder, because this task lives inside a ./tasks folder
    // If that's not your case, just use `__dirname`
    var folder = __dirname;

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['src/**/*.js'],
                dest: 'dist/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        qunit: {
            files: ['test/**/*.html']
        },
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
            options: {
                // options here to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        watch: {
            files: ['<%= jshint.files %>'],
            tasks: ['jshint', 'qunit']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html'
                    ]
                },
                options: {
                    server: {
                        baseDir: "./",
                        middleware: function(req, res, next) {
                            var fileName = url.parse(req.url);
                            fileName = fileName.href.split(fileName.search).join("");
                            var fileExists = fs.existsSync(folder + fileName);
                            if (!fileExists && fileName.indexOf("browser-sync-client") < 0) {
                                req.url = "/" + defaultFile;
                            }
                            return next();
                        }
                    }
                }
            }
        }
    });

    //grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-qunit');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-contrib-concat');
    //grunt.loadNpmTasks('grunt-serve');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('serve', ['browserSync']);

    //grunt.registerTask('test', ['jshint', 'qunit']);

    //grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);

};