(function () {
    angular
        .module('homeAutomation.house')
        .controller('HouseController', HouseController);

    HouseController.$inject = ['dataservice', '$log'];

    function HouseController(dataservice, $log) {
        var vm = this;
        vm.house = {};

        init();

        /**
         * Init the HouseController
         */
        function init() {
            //TODO:Move this into a service
            return getHouse().then(function () {
                $log.info('Activated House View');
            });
        }

        /**
         * Get the house and its associated features
         */
        function getHouse() {
            return dataservice.getHouse()
                .then(function (data) {
                    vm.house = data;
                    return vm.house;
                });
        }
    }
})();