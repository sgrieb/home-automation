

(function () {
    /**
    * @desc house for the base app
    * @example <div house></div>
    */
    angular
        .module('homeAutomation.house')
        .directive('house', house);

    function house() {
        var directive = {
            link: link,
            templateUrl: '/js/house/house.html',
            restrict: 'EA'
        };

        function link(scope, element, attrs) {

            //This is kinda ugly but it works
            //Set up the side nav
            $('#side-menu').metisMenu();
            
            
        }

        return directive;
    }
})();