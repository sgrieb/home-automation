(function () {
    angular
        .module('homeAutomation.core')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['config', '$log'];

    function NavbarController(config, $log) {
        var vm = this;
        vm.houseName = '';

        init();

        /**
         * Init the NavbarController
         */
        function init() {
            vm.houseName = config.house.name;
        }

    }
})();