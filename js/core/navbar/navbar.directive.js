angular.module('homeAutomation.core', [])
    .directive('haNavbar', function() {
        return {
            restrict:'E',
            templateUrl: '/js/core/navbar/navbar.html'
        };
    });