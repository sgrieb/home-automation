(function () {
    angular
    .module('homeAutomation.core')
    .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', 'config', '$log'];

    function dataservice($http, config, $log) {
        return {
            getHouse: getHouse
        };

        /**
         * Get a House and its associated features
         */
        function getHouse() {
            return $http.get(config.apiUrl + '/getHouse')
                .then(getHouseComplete)
                .catch(getHouseFailed);

            function getHouseComplete(response) {
                return response.data;
            }

            function getHouseFailed(error) {
                $log.error('XHR Failed for getHouse.' + error.data);
            }
        }
    }
})();