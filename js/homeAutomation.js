(function() {
    'use strict';
    
    angular.module('homeAutomation', [
            'homeAutomation.core',
            'homeAutomation.dashboard',
            'homeAutomation.lights',
            'ui.router'
        ])
        .config(function($stateProvider, $urlRouterProvider, $locationProvider, $provide) {
        
            // For any unmatched url, redirect to /dashboard
            $urlRouterProvider.otherwise("/dashboard");

            $locationProvider.html5Mode(true);
        })
        .run(['dataservice', '$log', '$state', 'config', function (dataservice, $log, $state, config) {
            
            //get base app data
            dataservice.getHouse().then(function(data){
                    //set it for general use
                    config.house = data;

                    //Loads the correct sidebar on window load,
                    //collapses the sidebar on window resize.
                    // Sets the min-height of #page-wrapper to window size
                    $(window).bind("load resize", function () {
                        var topOffset = 50;
                        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
                        if (width < 768) {
                            $('div.navbar-collapse').addClass('collapse');
                            topOffset = 100; // 2-row-menu
                        } else {
                            $('div.navbar-collapse').removeClass('collapse');
                        }

                        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
                        height = height - topOffset;
                        if (height < 1) height = 1;
                        if (height > topOffset) {
                            $("#page-wrapper").css("min-height", (height) + "px");
                        }
                    });

                    var url = window.location;
                    var element = $('ul.nav a').filter(function () {
                        return this.href == url || url.href.indexOf(this.href) == 0;
                    }).addClass('active').parent().parent().addClass('in').parent();
                    if (element.is('li')) {
                        element.addClass('active');
                    }
                    
                    //Done loading app
                    //go to default state
                    $state.go('dashboard');
                    $log.info('Successfully Booted HomeAutomation');  
                });
            
        }]);
        
})();