(function () {
    angular
        .module('homeAutomation.dashboard', [
            'ui.router'
        ])
        .config(
            ['$stateProvider',
                function ($stateProvider) {
                    // Now set up the states
                    $stateProvider
                        .state('dashboard', {
                            url: "/dashboard",
                            templateUrl: "js/dashboard/dashboard.html"
                        });
                }
           ]);
})();