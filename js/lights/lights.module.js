(function() {
    angular
        .module('homeAutomation.lights', [
            'ui.router'
        ])
        .config(
        ['$stateProvider',
            function ($stateProvider) {
                // Now set up the states
                $stateProvider
                    .state('lights', {
                        url: "/lights",
                        templateUrl: "js/lights/lights.html"
                    });
            }
        ]);
})();